function y = !_invoke_(varargin)
    y = !!_invoke_l(varargin);
endfunction
