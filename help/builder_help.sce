// Copyright INRIA 2008
// This file is released into the public domain

help_dir = get_absolute_file_path('builder_help.sce');

xmltojar(help_dir + "/en_US", TOOLBOX_TITLE, "en_US");

clear help_dir;
