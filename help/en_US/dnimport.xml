<?xml version="1.0" encoding="UTF-8"?>

<!--
*
* JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
* Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
*
* This file must be used under the terms of the CeCILL.
* This source file is licensed as described in the file COPYING, which
* you should have received as part of this distribution.  The terms
* are also available at
* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
*
*
-->

<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="dnimport">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>dnimport</refname>
    <refpurpose>Import a .NET class</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      dnimport(className)
      cl = dnimport(className, isAClassReturned)
    </synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>className</term>
        <listitem>
          <para>A string giving the class name</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>isAClassReturned</term>
        <listitem>
          <para>A boolean to indicate if a class object must be returned</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>The class must be in one of the loaded assemblies. During dNIMS initialization only mscorlib.dll and System.dll are loaded. To load assemblies, use <literal>dnload</literal> to load assemblies with a given qualified name or <literal>dnloadfrom</literal> to load assembly from given path.
    </para>
    <para>
      When <literal>dnimport("System.String")</literal>, a mlist named String is created on the Scilab stack. The created mlist has a type equals to _EClass with _EnvId equal to 1 and can be used to instantiate new objects in using something like <literal>str = String.new("A Scilab String");</literal>. It is possible to call the static methods (if exist) of the class
    </para>
    <programlisting role="example">
      <![CDATA[
               dnimport System.System;
               dnimport System.DateTime;               
               l = DateTime.Now.Ticks;
               d = DateTime.new(l)
               dnremove l, d;
      ]]></programlisting>
      <para>
        To avoid confusion with already existing Scilab variables, the second argument isAClassReturned can be used
      </para>
      <programlisting role="example">
        <![CDATA[
                 String = "Hello";
                 foo = dnimport("System.String", %f);
                 obj = foo.new("A string...")
                 dnremove obj;
        ]]></programlisting>
        <para>
          A class can be reloaded if it is allowed by the function <literal>jallowReloadClass</literal>.
        </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example">
      <![CDATA[
               dnload('System.Windows.Forms, Version = v2.0.50727, Culture = Neutral, PublicKeyToken = b77a5c561934e089');
               dnimport System.Windows.Forms.Label;
			   dnimport System.Windows.Forms.Form;

               dnform = Form.new("Hello World !! Window");
               dnlabel = Label.new("A JLabel containing ""Hello Wolrd""");
               cp = dnform.Components;
               cp.Add(dnlabel);               
               dnform.Show();
               dnremove dnlabel cp dnframe;
      ]]></programlisting>
  </refsection>
  <refsection role="see also">
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="allowClassReloading">jallowClassReloading</link>
      </member>
      <member>
        <link linkend="dnremove">jremove</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Author</title>
    <simplelist type="vert">
      <member>Calixte Denizet</member>
    </simplelist>
  </refsection>
</refentry>
