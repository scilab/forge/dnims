// ====================================================================
// Allan CORNET - DIGITEO - 2011
// This file is released into the public domain
// ====================================================================
function builder_src()
  //use first call if you want to compile managed c++ code (otherwise, precompiled dll will be used
  //tbx_builder_src_lang(["mcpp", "cpp", "c"],get_absolute_file_path("builder_src.sce"));
  tbx_builder_src_lang(["cpp", "c"],get_absolute_file_path("builder_src.sce"));
endfunction
builder_src();
clear builder_src;
