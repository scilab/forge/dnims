/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "ScilabObjects.h"
#include "JIMS.h"
#include "api_scilab.h"
#include "stack-c.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "OptionsHelper.h"
#include "call_scilab.h"
/*--------------------------------------------------------------------------*/
#define _INVOKE_ "!_invoke_"
#define COPYINVOCATIONMACROTOSTACK "copyInvocationMacroToStack"
/*--------------------------------------------------------------------------*/

static char isInit = 0;
void initialization(const char* fname)
{
    if (!isInit)
    {
        int* mlistaddr = NULL;
        const char* fields[] = {"_EVoid"};
        int id[nsiz];
        int tops;
        SciErr err;

		//TODO: initialize null and void for abstract environment
        createNamedEnvironmentObjectEnv(_ENVOBJ, "jnull", 0, 0);
		createNamedEnvironmentObjectEnv(_ENVOBJ, "dnnull", 0, 1);

        err = createNamedMList(pvApiCtx, "envvoid", 1, &mlistaddr);
        if (err.iErr)
        {
            printError(&err, 0);
            return;
        }

        err = createMatrixOfStringInNamedList(pvApiCtx, "envvoid", mlistaddr, 1, 1, 1, fields);
        if (err.iErr)
        {
            printError(&err, 0);
            return;
        }

		initializeenvironmentfactory();

		if(fname)
		{
			char* errmsg = NULL;
			initializeenvironment(fname, &errmsg);
		}

        // to push the macro !_invoke_ on the stack
        C2F(cvname)(id, _INVOKE_, &ZERO, strlen(_INVOKE_));
        Fin = 0;
        tops = Top;
        Top = Top - Rhs + 2;
        C2F(funs)(id);
        Top = tops;
        Fin = 0;
        C2F(com).fun = 0;

        isInit = 1;
    }
}

/**
 * Create a named external Environment Object (_EObj or _EClass) on the stack
 */
int createNamedEnvironmentObjectEnv(int type, const char *name, int id, const int envId)
{
    const char **fields;
    int* mlistaddr = NULL;
    SciErr err;

    switch (type)
    {
    case _ENVOBJ:;
        fields = _EObj;
        break;
    case _ENVCLASS:;
        fields = _EClass;
        break;
    default :;
        fields = _EObj;
        break;
    }

    err = createNamedMList(pvApiCtx, name, 3, &mlistaddr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    err = createMatrixOfStringInNamedList(pvApiCtx, name, mlistaddr, 1, 1, 3, fields);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if(envId < 0)
    {
        return 0;
    }
    err = createMatrixOfInteger32InNamedList(pvApiCtx, name, mlistaddr, 2, 1, 1, &envId);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    err = createMatrixOfInteger32InNamedList(pvApiCtx, name, mlistaddr, 3, 1, 1, &id);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    return 1;
}

/**
 * Create a named external Environment Object (_EObj or _EClass) on the stack
 */
int createNamedEnvironmentObject(int type, const char *name, int id, const char* fname)
{
    return createNamedEnvironmentObjectEnv(type, name, id, getEnvironmentId(fname));
}
/*--------------------------------------------------------------------------*/
/**
 * Create a External Environment Object at the given position on the stack
 */
int createEnvironmentObjectAtPosEnv(int type, int pos, int id, const int envId)
{
	const char **fields = NULL;
	int* mlistaddr = NULL;
	SciErr err;

	switch (type)
	{
	case _ENVOBJ:;
		fields = _EObj;
		break;
	case _ENVCLASS:;
		fields = _EClass;
		break;
	default :;
		fields = _EObj;
		break;
	}

	err = createMList(pvApiCtx, pos, 3, &mlistaddr);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = createMatrixOfStringInList(pvApiCtx, pos, mlistaddr, 1, 1, 3, fields);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	if(envId < 0)
	{
		return 0;
	}

	err = createMatrixOfInteger32InList(pvApiCtx, pos, mlistaddr, 2, 1, 1, &envId);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = createMatrixOfInteger32InList(pvApiCtx, pos, mlistaddr, 3, 1, 1, &id);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	return 1;
}
/*--------------------------------------------------------------------------*/
/**
 * Create a External Environment Object at the given position on the stack
 */
int createEnvironmentObjectAtPos(int type, int pos, int id, const char* fname)
{
    return createEnvironmentObjectAtPosEnv(type, pos, id, getEnvironmentId(fname));
}
/*--------------------------------------------------------------------------*/
/**
 * When obj.method(...) is invoked, the extraction of 'method' field is followed by a copy
 * of the macro !_invoke_, so obj.method(...) would be equivalent to !_invoke_(...)
 */
void copyInvocationMacroToStack(int pos)
{
    static int init = 0;
    static int id[nsiz];
    int dest = 0;

    if (!init)
    {
        C2F(str2name)(_INVOKE_, id, strlen(_INVOKE_));
        init = 1;
    }

    Fin = -1;
    C2F(stackg)(id);
    if (Fin == 0)
    {
        isInit = 0;
        initialization(0);
        Fin = -1;
        C2F(stackg)(id);
    }

    dest = Top - Rhs + pos;
    C2F(vcopyobj)(COPYINVOCATIONMACROTOSTACK, &Fin, &dest, strlen(COPYINVOCATIONMACROTOSTACK));
    setCopyOccured(1);
}
/*--------------------------------------------------------------------------*/
void removeTemporaryVars(char *fname, int *tmpvar)
{
	return removeTemporaryVarsEnv(getEnvironmentId(fname), tmpvar);
}
/*--------------------------------------------------------------------------*/
void removeTemporaryVarsEnv(const int envId, int *tmpvar)
{
	int i = 1;
	for (; i <= *tmpvar; i++)
	{
		removescilabobjectenv(envId, tmpvar[i]);
	}
}
/*--------------------------------------------------------------------------*/
int removeVar(const char *fname, int *addr, int pos, int intIsEnvOrClass)
{
    SciErr err;
    int type, row, col, *id;

    err = getVarType(pvApiCtx, addr, &type);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (type == sci_mlist && intIsEnvOrClass)
    {
        err = getMatrixOfInteger32InList(pvApiCtx, addr, 3, &row, &col, &id);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        removescilabobject(fname, *id);
        return 1;
    }
    else if (type == sci_strings)
    {
        char *varName = NULL;
        if (getAllocatedSingleString(pvApiCtx, addr, &varName))
        {
            return 0;
        }

        err = getVarAddressFromName(pvApiCtx, varName, &addr);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        err = getVarType(pvApiCtx, addr, &type);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        if (type == sci_mlist && (isEnvObj(addr) || isEnvClass(addr)))
        {
            err = getMatrixOfInteger32InList(pvApiCtx, addr, 3, &row, &col, &id);
            if (err.iErr)
            {
                printError(&err, 0);
                return 0;
            }

            removescilabobject(fname, *id);
            return 1;
        }
    }

    Scierror(999, "%s: Wrong type for input argument %i: a _EObj or a _EClass expected", fname, pos);
    return 0;
}
/*--------------------------------------------------------------------------*/
int unwrap(int idObj, int pos, const char *fname)
{
	return unwrapenv(idObj, pos, getEnvironmentId(fname));
}
/*--------------------------------------------------------------------------*/
/**
 * Unwrap !
 */
int unwrapenv(int idObj, int pos, const int envId)
{
    char *errmsg = NULL;
    int type = 0;

    if (idObj == 0) // null object
    {
        double *arr = NULL;
        SciErr err = allocMatrixOfDouble(pvApiCtx, pos, 0, 0, &arr);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }
        return 1;
    }

    type = isunwrappable(envId, idObj, &errmsg);
    if (errmsg)
    {
        Scierror(999,"%s: %s\n", "unwrap", errmsg);
        return 0;
    }

    switch (type)
    {
    case -1:;
        return 0;
    case 0:;
        unwrapdouble(idObj, pos, &errmsg, envId);
        break;
    case 1:;
        unwraprowdouble(idObj, pos, &errmsg, envId);
        break;
    case 2:;
        unwrapmatdouble(idObj, pos, &errmsg, envId);
        break;
    case 3:;
        unwrapstring(idObj, pos, &errmsg, envId);
        break;
    case 4:;
        unwraprowstring(idObj, pos, &errmsg, envId);
        break;
    case 5:;
        unwrapmatstring(idObj, pos, &errmsg, envId);
        break;
    case 6:;
        unwrapint(idObj, pos, &errmsg, envId);
        break;
    case 7:;
        unwraprowint(idObj, pos, &errmsg, envId);
        break;
    case 8:;
        unwrapmatint(idObj, pos, &errmsg, envId);
        break;
    case 9:;
        unwrapboolean(idObj, pos, &errmsg, envId);
        break;
    case 10:;
        unwraprowboolean(idObj, pos, &errmsg, envId);
        break;
    case 11:;
        unwrapmatboolean(idObj, pos, &errmsg, envId);
        break;
    case 12:;
        unwrapbyte(idObj, pos, &errmsg, envId);
        break;
    case 13:;
        unwraprowbyte(idObj, pos, &errmsg, envId);
        break;
    case 14:;
        unwrapmatbyte(idObj, pos, &errmsg, envId);
        break;
    case 15:;
        unwrapshort(idObj, pos, &errmsg, envId);
        break;
    case 16:;
        unwraprowshort(idObj, pos, &errmsg, envId);
        break;
    case 17:;
        unwrapmatshort(idObj, pos, &errmsg, envId);
        break;
    case 18:;
        unwrapchar(idObj, pos, &errmsg, envId);
        break;
    case 19:;
        unwraprowchar(idObj, pos, &errmsg, envId);
        break;
    case 20:;
        unwrapmatchar(idObj, pos, &errmsg, envId);
        break;
    case 21:;
        unwrapfloat(idObj, pos, &errmsg, envId);
        break;
    case 22:;
        unwraprowfloat(idObj, pos, &errmsg, envId);
        break;
    case 23:;
        unwrapmatfloat(idObj, pos, &errmsg, envId);
        break;
    case 24:;
        unwraplong(idObj, pos, &errmsg, envId);
        break;
    case 25:;
        unwraprowlong(idObj, pos, &errmsg, envId);
        break;
    case 26:;
        unwrapmatlong(idObj, pos, &errmsg, envId);
        break;
    case 27:;
        unwrapdouble(idObj, pos, &errmsg, envId);
        break;
    case 28:;
        unwrapint(idObj, pos, &errmsg, envId);
        break;
    case 29:;
        unwraplong(idObj, pos, &errmsg, envId);
        break;
    case 30:;
        unwrapbyte(idObj, pos, &errmsg, envId);
        break;
    case 31:;
        unwrapchar(idObj, pos, &errmsg, envId);
        break;
    case 32:;
        unwrapboolean(idObj, pos, &errmsg, envId);
        break;
    case 33:;
        unwrapfloat(idObj, pos, &errmsg, envId);
        break;
    case 34:;
        unwrapshort(idObj, pos, &errmsg, envId);
        break;
    default:;
        return 0;
    }

    if (errmsg)
    {
        Scierror(999,"%s: %s\n", "unwrap", errmsg);
        return 0;
    }

    return 1;
}
/*--------------------------------------------------------------------------*/
