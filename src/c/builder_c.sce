// Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
// Copyright (C) 2011 - Allan CORNET  
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
function builder_c()
  here = get_absolute_file_path("builder_c.sce");
  src_c = ["ScilabObjects.c", ..
           "getSciArgs.c"];

  if getos() == "Windows" then
    src_c = [src_c, ..
             "dllMain.c"];
  end

  CFLAGS = "-I" + fullpath(here + "../include/");
  CFLAGS = CFLAGS + " -I" + fullpath(here);
  CFLAGS = CFLAGS + " -I" + fullpath(SCI + "/modules/call_scilab/includes");

  tbx_build_src('jims_c', src_c, 'c', here, '', '', CFLAGS);
endfunction
builder_c();
clear builder_c;
