/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2011 - Allan CORNET
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#ifndef __GETSCIARGS_H__
#define __GETSCIARGS_H__

#define __USE_DEPRECATED_STACK_FUNCTIONS__

/*--------------------------------------------------------------------------*/
int compareStrToMlistType(char *str, int *mlist);
int isEnvObj(int *mlist);
int isEnvClass(int *mlist);
int isEnvVoid(int *mlist);
char* getSingleString(int pos, const char *fname);
int isPositiveIntegerAtAddress(int *addr);
int getIdOfArg(int *addr, const char *fname, int *tmpvars, char isClass, int pos);
int getIdOfArgEnv(int *addr, const char *fname, int *tmpvars, char isClass, int pos, int envId);
int getIdOfEnvironemnt(int *addr, const char *fname, char isClass, int pos);
int getIdOfArgAsDirectBuffer(int pos, const char *fname, char forceByteBuffer, void **tmpref);
/*--------------------------------------------------------------------------*/
#endif /* __GETSCIARGS_H__ */
