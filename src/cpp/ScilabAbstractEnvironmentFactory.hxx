#ifndef __SCILABABSTRACTENVIRONMENTFACTORY_H__
#define __SCILABABSTRACTENVIRONMENTFACTORY_H__

#include <map>
#include <stdio.h>
#include <string.h>

#include "ScilabAbstractEnvironmentException.hxx"
#include "ScilabAbstractEnvironment.hxx"
#include "ScilabAbstractEnvironmentWrapper.hxx"

enum EnvironmentType
{
	JAVA_ENVIRONMENT,
	DOTNET_ENVIRONMENT
};

struct strComparator
{
	bool operator() (const std::string lhs, const std::string rhs) const
	{return strcmp(lhs.c_str(), rhs.c_str()) > 0;}
};

class ScilabAbstractEnvironmentFactory
{
	private:
		static std::map<std::string, EnvironmentType, strComparator> macrosMap_;
		static std::map<EnvironmentType, ScilabAbstractEnvironment*> environmentsMap_;
		static std::map<EnvironmentType, ScilabAbstractEnvironmentWrapper*> wrappersMap_;
		static std::map<EnvironmentType, char*> environmentNamesMap_;
		static char* unknownEnvString;
	public:
		static int getEnvironmentId(const char* fname );
		static ScilabAbstractEnvironment* getEnvironment(EnvironmentType envType);
		static ScilabAbstractEnvironment* getEnvironmentByMacroName(const char* fname);
		static ScilabAbstractEnvironmentWrapper* getEnvironmentWrapperByMacroName(const char* fname);
		static ScilabAbstractEnvironmentWrapper* getEnvironmentWrapper(EnvironmentType envType);
		static void initializeEnvironment(const char* fname);

		static void Initialize();
		static char* getEnvironmentName(const EnvironmentType& envId);
};

#endif //__SCILABABSTRACTENVIRONMENTFACTORY_H__
