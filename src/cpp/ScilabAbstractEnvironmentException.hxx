#ifndef __SCILABABSTRACTENVIRONMENTEXCEPTION_H__
#define __SCILABABSTRACTENVIRONMENTEXCEPTION_H__

#include <exception>
#include <string>

class ScilabAbstractEnvironmentException : public std::exception
{

private:
	std::string message_;

public:
	ScilabAbstractEnvironmentException(const char* message)
		:message_(message){};

	inline std::string GetDescription() { return message_; }

	~ScilabAbstractEnvironmentException() throw() {};
};

#endif //__SCILABABSTRACTENVIRONMENTEXCEPTION_H__