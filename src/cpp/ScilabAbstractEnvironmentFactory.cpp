#define UNK_MACRO "Unknown macro"

#include <stdio.h>
#include <string.h>

#include "ScilabAbstractEnvironmentFactory.hxx"
#include "ScilabJavaEnvironment.hxx"
#include "ScilabJavaEnvironmentWrapper.hxx"
#if defined(WIN32) || defined(WIN64)
#include "ScilabDotNetEnvironment.hxx"
#include "ScilabDotNetEnvironmentWrapper.hxx"
#endif

std::map<std::string, EnvironmentType, strComparator> ScilabAbstractEnvironmentFactory::macrosMap_;
std::map<EnvironmentType, ScilabAbstractEnvironment*> ScilabAbstractEnvironmentFactory::environmentsMap_;
std::map<EnvironmentType, ScilabAbstractEnvironmentWrapper*> ScilabAbstractEnvironmentFactory::wrappersMap_;
std::map<EnvironmentType, char*> ScilabAbstractEnvironmentFactory::environmentNamesMap_;
char* ScilabAbstractEnvironmentFactory::unknownEnvString;

void ScilabAbstractEnvironmentFactory::Initialize()
{
	if(macrosMap_.size() == 0)
	{
		macrosMap_["jdeff"] = JAVA_ENVIRONMENT;
		macrosMap_["jexists"] = JAVA_ENVIRONMENT;
		macrosMap_["jenableTrace"] = JAVA_ENVIRONMENT;
		macrosMap_["jdisableTrace"] = JAVA_ENVIRONMENT;
		macrosMap_["jcast"] = JAVA_ENVIRONMENT;
		macrosMap_["jarray"] = JAVA_ENVIRONMENT;
		macrosMap_["jwrap"] = JAVA_ENVIRONMENT;
		macrosMap_["jwrapinfloat"] = JAVA_ENVIRONMENT;
		macrosMap_["jwrapinchar"] = JAVA_ENVIRONMENT;
		macrosMap_["jimport"] = JAVA_ENVIRONMENT;
		macrosMap_["jnewInstance"] = JAVA_ENVIRONMENT;
		macrosMap_["jinvoke"] = JAVA_ENVIRONMENT;
		macrosMap_["jinvoke_db"] = JAVA_ENVIRONMENT;
		macrosMap_["jsetfield"] = JAVA_ENVIRONMENT;
		macrosMap_["jgetfield"] = JAVA_ENVIRONMENT;
		macrosMap_["jgetfield"] = JAVA_ENVIRONMENT;
		macrosMap_["jgetfields"] = JAVA_ENVIRONMENT;
		macrosMap_["junwrap"] = JAVA_ENVIRONMENT;
		macrosMap_["junwraprem"] = JAVA_ENVIRONMENT;
		macrosMap_["jautoUnwrap"] = JAVA_ENVIRONMENT;
		macrosMap_["jremove"] = JAVA_ENVIRONMENT;
		macrosMap_["jgetmethods"] = JAVA_ENVIRONMENT;
		macrosMap_["jgetrepresentation"] = JAVA_ENVIRONMENT;
		macrosMap_["jgetclassname"] = JAVA_ENVIRONMENT;
		macrosMap_["jinit"] = JAVA_ENVIRONMENT;
		macrosMap_["jcompile"] = JAVA_ENVIRONMENT;

#if defined(WIN32) || defined(WIN64)
		macrosMap_["dndeff"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnexists"] = DOTNET_ENVIRONMENT;
		macrosMap_["dncast"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnarray"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnwrap"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnwrapinfloat"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnwrapinchar"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnimport"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnnewInstance"] = DOTNET_ENVIRONMENT;
		macrosMap_["dninvoke"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnsetfield"] = DOTNET_ENVIRONMENT;
		macrosMap_["dngetfield"] = DOTNET_ENVIRONMENT;
		macrosMap_["dngetfield"] = DOTNET_ENVIRONMENT;
		macrosMap_["dngetfields"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnunwrap"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnunwraprem"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnautoUnwrap"] = DOTNET_ENVIRONMENT;
		macrosMap_["dnremove"] = DOTNET_ENVIRONMENT;
		macrosMap_["dngetmethods"] = DOTNET_ENVIRONMENT;
		macrosMap_["dngetrepresentation"] = DOTNET_ENVIRONMENT;
		macrosMap_["dngetclassname"] = DOTNET_ENVIRONMENT;
		macrosMap_["dninit"] = DOTNET_ENVIRONMENT;
		macrosMap_["dncompile"] = DOTNET_ENVIRONMENT;
#endif

		environmentsMap_.clear();

		unknownEnvString = strdup("Unknown environment");

		environmentNamesMap_[JAVA_ENVIRONMENT] = strdup("Scilab Java Environment");
		environmentNamesMap_[DOTNET_ENVIRONMENT] = strdup("Scilab .NET Environment");
	}
}

ScilabAbstractEnvironment* ScilabAbstractEnvironmentFactory::getEnvironment(EnvironmentType envType)
{
	if(environmentsMap_.find(envType) == environmentsMap_.end())
	{
		switch(envType)
		{
			case JAVA_ENVIRONMENT:
				environmentsMap_[JAVA_ENVIRONMENT] = new ScilabJavaEnvironment();
				break;
				
			case DOTNET_ENVIRONMENT:
#if defined(WIN32) || defined(WIN64)
				environmentsMap_[DOTNET_ENVIRONMENT] = new ScilabDotNetEnvironment();
#else
				throw ScilabAbstractEnvironmentException("Scilab .NET environment suported only on Windows platform");
#endif
				break;
		}
	}
	
	return environmentsMap_[envType];
}

ScilabAbstractEnvironmentWrapper* ScilabAbstractEnvironmentFactory::getEnvironmentWrapper(EnvironmentType envType)
{
	if(wrappersMap_.find(envType) == wrappersMap_.end())
	{
		switch(envType)
		{
		case JAVA_ENVIRONMENT:
			wrappersMap_[JAVA_ENVIRONMENT] = new ScilabJavaEnvironmentWrapper();
			break;

		case DOTNET_ENVIRONMENT:
#if defined(WIN32) || defined (WIN64)
			wrappersMap_[DOTNET_ENVIRONMENT] = new ScilabDotNetEnvironmentWrapper();
#else
			throw ScilabAbstractEnvironmentException("Scilab .NET environment suported only on Windows platform");
#endif
			break;
		}
	}

	return wrappersMap_[envType];
}

ScilabAbstractEnvironmentWrapper* ScilabAbstractEnvironmentFactory::getEnvironmentWrapperByMacroName(const char* fname)
{
	if (macrosMap_.find(fname) == macrosMap_.end())
	{
		throw ScilabAbstractEnvironmentException(UNK_MACRO);
		return NULL;
	}
	
	EnvironmentType envType = macrosMap_[fname];

	return getEnvironmentWrapper(envType);
}

ScilabAbstractEnvironment* ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(const char* fname)
{
	if (macrosMap_.find(fname) == macrosMap_.end())
	{
		throw ScilabAbstractEnvironmentException(UNK_MACRO);
		return NULL;
	}
	
	EnvironmentType envType = macrosMap_[fname];
	
	return getEnvironment(envType);
}

int ScilabAbstractEnvironmentFactory::getEnvironmentId(const char* fname)
{
	if(macrosMap_.find(fname) != macrosMap_.end())
	{
		return (int)macrosMap_[fname];
	}
	else
	{
		throw ScilabAbstractEnvironmentException(UNK_MACRO);
	}
}

void ScilabAbstractEnvironmentFactory::initializeEnvironment(const char* fname)
{
	if(macrosMap_.find(fname) != macrosMap_.end())
	{
		if(environmentsMap_.find(macrosMap_[fname]) != environmentsMap_.end())
		{
			environmentsMap_[macrosMap_[fname]] = getEnvironment(macrosMap_[fname]);
		}
	}
	else
	{
		throw ScilabAbstractEnvironmentException(UNK_MACRO);
	}
}

char* ScilabAbstractEnvironmentFactory::getEnvironmentName(const EnvironmentType& envId)
{
	if(environmentNamesMap_.find(envId) != environmentNamesMap_.end())
	{
		return environmentNamesMap_[envId];
	}
	else
	{
		return unknownEnvString;
	}
}