#using <mscorlib.dll>

using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

__gc class ScilabDotNetType
{
public:

    Type* type_;
    List<String*>* methodSignatures_;
    List<String*>* methodNames_;
    List<String*>* fieldNames_;
    List<String*>* staticMethodSignatures_;
    List<String*>* staticMethodNames_;
    List<String*>* staticFieldNames_;

    ScilabDotNetType(Type* aType);
    
    bool HasMethod(String* methodName);
    
    bool HasField(String* fieldName);

    List<String*>* GetAccessibleMethodNames();

    List<String*>* GetAccessibleFieldNames();

    List<String*>* GetAccessibleStaticFieldNames();

    List<String*>* GetAccessibleStaticMethodNames();
};