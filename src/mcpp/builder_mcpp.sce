// ====================================================================
// Igor GRIDCHYN
// DIGITEO 2011
// This file is released into the public domain
// ====================================================================

curdir = pwd()
folder = get_absolute_file_path('builder_mcpp.sce')
chdir(folder)

dos('cl /I""../cpp"" /Zi /clr:oldSyntax /nologo /W3 /WX- /O2 /Oi /Oy- /GL /D ""_WINDLL"" /D ""_MBCS"" /Gm- /EHa /GS /Gy /fp:precise /Zc:wchar_t /Zc:forScope /Gd /analyze- /errorReport:queue /c   ScilabDotNetType.cpp')

dos('cl /I""../cpp"" /Zi /clr:oldSyntax /nologo /W3 /WX- /O2 /Oi /Oy- /GL /D ""_WINDLL"" /D ""_MBCS"" /Gm- /EHa /GS /Gy /fp:precise /Zc:wchar_t /Zc:forScope /Gd /analyze- /errorReport:queue /c   ScilabDotNetObjects.cpp')

dos('link /OUT:""ScilabDotNetObjects.dll"" /NOLOGO /DLL ""kernel32.lib"" ""user32.lib"" ""gdi32.lib"" ""winspool.lib"" ""comdlg32.lib"" ""advapi32.lib"" ""shell32.lib"" ""ole32.lib"" ""oleaut32.lib"" ""uuid.lib"" ""odbc32.lib"" ""odbccp32.lib"" /OPT:REF /OPT:ICF /LTCG /TLBID:1 /DYNAMICBASE /NXCOMPAT /IMPLIB:""ScilabDotNetObjects.lib"" /MACHINE:X86 /ERRORREPORT:QUEUE ScilabDotNetObjects.obj ScilabDotNetType.obj')

dos('del *.obj /q')
dos('del *.manifest /q')
dos('del *.pdb /q')
dos('del *.exp /q')

chdir(curdir)