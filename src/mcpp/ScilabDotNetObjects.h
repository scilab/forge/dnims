#ifndef SDNO_LINKAGE
#define SDNO_LINKAGE  __declspec(dllimport)
#endif

/*--------------------------------------------------------------------------*/
#ifdef _MSC_VER
#include <windows.h>
#endif

#ifndef _MSC_VER /* Defined anyway with Visual */
#if !defined(byte)
typedef signed char byte;
#else
#pragma message("Byte has been redefined elsewhere. Some problems can happen")
#endif
#endif
/* ------------------------------------------------------------------------- */

#define DNUnwrapForType(Type, type) static type unwrap##Type(const int& id); \
    static type* unwrapRow##Type(const int& id, int& len); \
    static type* unwrapMat##Type(const int& id, int& lenCol, int& lenRow, int convMatrixMethod);

class SDNO_LINKAGE ScilabDotNetObjects
{
public:
    static void Initialize();
    static void LoadFrom(const char* dllPath, bool allowReload);
    static void Load(const char* longName, bool allowReload);
    static int NewInstance(int classId, int* argv, int argc);
    static int LoadClass(char* typeName, bool allowReload);
    static bool IsValidDotNetObject(int id);
    static bool RemoveDotNetObject(int id);
    static char* GetObjectRepresentation(int id);
    static void GarbageCollect();
    static int CreateArray(char* className, int* dims, int len);
    static int Invoke(int id, char* methodName, int* argv, int argc);
    static void SetField(int id, char *fieldName, int idArg);
    static int GetField(int id, char *fieldName);
    static int GetFieldType(int id, char* fieldName);
    static int GetArrayElement(int id, int* index, int length);
    static void SetArrayElement(int id, int* index, int length, int idArg);
    static int Cast(int id, char* className);
    static int CastWithId(int id, int typeId);
    static char** GetAccessibleMethods(int id, int& length);
    static char** GetAccessibleFields(int id, int& length);
    static char* GetDotNetClassName(int id);
    static int IsUnwrappable(int id);
    static int CompileCode(char* className, char** code, int size);

    static int wrapString(char* str);
    static int wrapString(char** strings, int len);
    static int wrapString(char*** strings, int rows, int cols);

    static int wrapDouble(double x);
    static int wrapDouble(double* x, const  int xSize);
    static int wrapDouble(double** x, const  int xSize, const int xSizeCol);

    static int wrapInt(int x);
    static int wrapInt(int* x, const int length);
    static int wrapInt(int** x, const int rowLengthes, const int columns);

    static int wrapUInt(long long x);
    static int wrapUInt(long long* x, const int length);
    static int wrapUInt(long long** x, const int rowLengthes, const int columns);

    static int wrapShort(short x);
    static int wrapShort(short* x, const int length);
    static int wrapShort(short** x, const int rowLengthes, const int columns);

    static int wrapUShort(int x);
    static int wrapUShort(int * x, const int length);
    static int wrapUShort(int ** x, const int rowLengthes, const int columns);

    static int wrapBoolean(bool x);
    static int wrapBoolean(bool* x, const int length);
    static int wrapBoolean(bool** x, const int rowLengthes, const int columns);

    static int wrapChar(unsigned short x);
    static int wrapChar(unsigned short* x, const int length);
    static int wrapChar(unsigned short** x, const int rowLengthes, const int columns);

    static int wrapFloat(float x);
    static int wrapFloat(float* x, const int length);
    static int wrapFloat(float** x, const int rowLengthes, const int columns);

    static int wrapLong(long long x);
    static int wrapLong(long long* x, const int length);
    static int wrapLong(long long** x, const int rowLengthes, const int columns);

    static int wrapByte(byte x);
    static int wrapByte(byte* x, const int length);
    static int wrapByte(byte** x, const int rowLengthes, const int columns);

    static int wrapUByte(short x);
    static int wrapUByte(short* x, const int length);
    static int wrapUByte(short** x, const int rowLengthes, const int columns);

    DNUnwrapForType(Double, double)
    DNUnwrapForType(Int, int)
    DNUnwrapForType(Short, short)
    DNUnwrapForType(Boolean, int)
    DNUnwrapForType(Char, unsigned short)
    DNUnwrapForType(Float, float)
    DNUnwrapForType(Byte, char)
    DNUnwrapForType(String, char*)
    #ifdef __SCILAB_INT64__
        DNUnwrapForType(Long, long)
    #else
        DNUnwrapForType(Long, unsigned int)
    #endif
};