/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SCILABOBJECTS_HXX__
#define __SCILABOBJECTS_HXX__
/*--------------------------------------------------------------------------*/
#include <jni.h>
#include "ScilabAbstractEnvironmentWrapper.hxx"
#include "ScilabAbstractEnvironmentFactory.hxx"

extern "C"
{
#define _GLOBAL_VARS_
#include "getScilabJavaVM.h"
#include "ScilabObjects.h"
} /* extern "C" */

/*
 * Structs defined to make easier the wrapping
 */
#define __JIMS_wrapToWrapObject__(Type,type) struct __JIMS__ScilabAnOtherWrapper##Type##__ { \
        static int wrap(type x, int envId)                             \
            {                                                           \
				ScilabAbstractEnvironmentWrapper* abstrWrapper = ScilabAbstractEnvironmentFactory::getEnvironmentWrapper((EnvironmentType)envId); \
				return abstrWrapper->wrap##Type(x); \
            };                                                          \
        static int wrap(type *x, int len, int envId)                   \
            {                                                           \
				ScilabAbstractEnvironmentWrapper* abstrWrapper = ScilabAbstractEnvironmentFactory::getEnvironmentWrapper((EnvironmentType)envId); \
                return abstrWrapper->wrap##Type(x, len); \
            };                                                          \
        static int wrap(type **xx, int r, int c, int envId)            \
            {                                                           \
				ScilabAbstractEnvironmentWrapper* abstrWrapper = ScilabAbstractEnvironmentFactory::getEnvironmentWrapper((EnvironmentType)envId); \
                return abstrWrapper->wrap##Type(xx, r, c); \
            };                                                          \
    };

__JIMS_wrapToWrapObject__(Double,double)
__JIMS_wrapToWrapObject__(Int,int)
__JIMS_wrapToWrapObject__(Byte,byte)
__JIMS_wrapToWrapObject__(Short,short)
__JIMS_wrapToWrapObject__(Char,unsigned short)
__JIMS_wrapToWrapObject__(UInt,long long)
__JIMS_wrapToWrapObject__(UByte,short)
__JIMS_wrapToWrapObject__(UShort,int)
__JIMS_wrapToWrapObject__(Boolean,bool)
__JIMS_wrapToWrapObject__(Float,float)
__JIMS_wrapToWrapObject__(Long,long long)

#define unwrap(kind,Kind,type,ScilabType,JNIType,Type) void unwrap##kind##type (int id, int pos, char **errmsg, int envId) \
    {                                                                   \
		try															\
		{															\
			ScilabAbstractEnvironmentWrapper* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentWrapper((EnvironmentType)envId); \
			abstrEnv->unwrap##kind##type (id, pos);					\
		}																\
		catch(ScilabAbstractEnvironmentException& envExcept)		\
		{															\
			*errmsg = strdup(envExcept.GetDescription().c_str());	\
		}															\
    }

#define unwrapForType(type,ScilabType,JNIType,Type) unwrap(,Single,type,ScilabType,JNIType,Type) \
    unwrap(row,Row,type,ScilabType,JNIType,Type)                        \
    unwrap(mat,Mat,type,ScilabType,JNIType,Type)


#define wrapSingleCall(type, Type) int wrapSingle##Type(type x, int envId)         \
    {                                                                   \
        return wrapSingle<type, __JIMS__ScilabAnOtherWrapper##Type##__>(x, envId); \
    }

#define wrapRowCall(type, Type) int wrapRow##Type(type *x, int len, int envId)     \
    {                                                                   \
        return wrapRow<type, __JIMS__ScilabAnOtherWrapper##Type##__>(x, len, envId); \
    }

#define wrapMatCall(type, Type) int wrapMat##Type(type *x, int r, int c, int envId) \
    {                                                                   \
        return wrapMat<type, __JIMS__ScilabAnOtherWrapper##Type##__>(x, r, c, envId); \
    }

#define wrapForType(type, Type) wrapSingleCall(type,Type)       \
    wrapRowCall(type,Type)                                      \
    wrapMatCall(type,Type)


#define wrapSingleWithCastCall(type, castType, Type) int wrapSingle##Type(type x, int envId) \
    {                                                                   \
        return wrapSingleWithCast<type, castType, __JIMS__ScilabAnOtherWrapper##Type##__>(x, envId); \
    }

#define wrapRowWithCastCall(type, castType, Type) int wrapRow##Type(type *x, int len, int envId) \
    {                                                                   \
        return wrapRowWithCast<type, castType, __JIMS__ScilabAnOtherWrapper##Type##__>(x, len, envId); \
    }

#define wrapMatWithCastCall(type, castType, Type) int wrapMat##Type(type *x, int r, int c, int envId) \
    {                                                                   \
        return wrapMatWithCast<type, castType, __JIMS__ScilabAnOtherWrapper##Type##__>(x, r, c, envId); \
    }

#define wrapWithCastForType(type, castType, Type) wrapSingleWithCastCall(type,castType,Type) \
    wrapRowWithCastCall(type,castType,Type)                             \
    wrapMatWithCastCall(type,castType,Type)

#define wrapIntoDirectBuffer(Type,type) void* wrapAsDirect##Type##Buffer(type *addr, long size, int *id) \
    {                                   \
    JavaVM *vm_ = getScilabJavaVM ();               \
    if (vm_)                            \
    {                               \
        return ScilabJavaObjectBis::wrapAsDirectBuffer<type>(vm_, addr, size, id); \
    }                               \
    }

/*--------------------------------------------------------------------------*/
#endif /* __SCILABOBJECTS_HXX__ */
