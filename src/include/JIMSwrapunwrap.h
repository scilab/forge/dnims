/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
/*
 * Provide several wrappers for Scilab's matrix allocation
 * Mainly used in unwrap.hpp
 */
/*--------------------------------------------------------------------------*/
#ifndef __JIMS__WRAPUNWRAP_H__
#define __JIMS__WRAPUNWRAP_H__

#define __JIMS_getmethod__(Kind, Type) static const char * get##Kind##MethodName() \
    {                                                                   \
        return "unwrap" #Kind #Type;                                    \
    };

#define __JIMS_getsignature__(Kind, Signature) static const char * get##Kind##MethodSignature() \
    {                                                                   \
        return "(I)" Signature;                                         \
    };

#define __JIMS_getvariableptr__() static T get##Kind##MethodSignature() \
    {                                                                   \
        return "(I)" Signature;                                         \
    };

#endif //__JIMS__WRAPUNWRAP_H__
/*--------------------------------------------------------------------------*/