//
//  Copyright (C) 2011 - DIGITEO - Allan CORNET
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// <-- JVM MANDATORY -->
//

if execstr("jcompile([""1"", ""1""], 2)", "errcatch") <> 999 then pause, end
if execstr("jcompile(""1"", 2)", "errcatch") <> 999 then pause, end

hw = jcompile("HelloWorld", ["public class HelloWorld {"
                             "public static String getHello() {"
                             "return ""Hello World !!"";"
                             "}"
                             "}"]);

if typeof(hw) <> "_EClass" then pause, end

hello = hw.getHello();
if typeof(hello) <> "_EObj" then pause, end
if hello.contentEquals(jwrap("Hello World !!")) <> jwrap(%t) then pause, end

jremove hello hw;

