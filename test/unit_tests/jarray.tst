//
//  Copyright (C) 2011 - DIGITEO - Allan CORNET
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// <-- JVM MANDATORY -->
//

if execstr("a = jarray();", "errcatch") <> 999 then pause, end
if execstr("a = jarray(3, 2, 2, 3);", "errcatch") <> 999 then pause, end

a = jarray("java.lang.String", 2, 2, 3);
if typeof(a) <> "_EObj" then pause, end

a(0, 0, 2) = "Hi Jims !";
b = a(0, 0, 2);
if typeof(b) <> "_EObj" then pause, end
if b.toString() <> "Hi Jims !" then pause, end

a(1, 0, 2) = "Hi Jims again !!";
b = a(1, 0, 2);
if typeof(b) <> "_EObj" then pause, end

if length(a(1, 0, 2)) <> 3 then pause, end
