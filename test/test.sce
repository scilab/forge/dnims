here = get_absolute_file_path("test.sce");
javaclasspath(here);

jimport test;

moc = ['cr' 'rc']
jautoUnwrap(%T);
N=30;

// Direct buffers
tic();
for i=1:N,for j=1:N,a=rand(i,j);b=a+1;jinvoke_db(test, "F", "a");if a <> b then,disp("bug");end;end;end
for i=1:N,for j=1:N,a=int8(10*rand(i,j));b=a+1;jinvoke_db(test, "F", "a");if a <> b then,disp("bug");end;end;end
for i=1:N,for j=1:N,a=int16(100*rand(i,j));b=a+1;jinvoke_db(test, "F", "a");if a <> b then,disp("bug");end;end;end
for i=1:N,for j=1:N,a=int32(1000*rand(i,j));b=a+1;jinvoke_db(test, "F", "a");if a <> b then,disp("bug");end;end;end
for i=1:N,for j=1:N,a=uint8(10*rand(i,j));b=a+1;jinvoke_db(test, "F", "a");if a <> b then,disp("bug");end;end;end
for i=1:N,for j=1:N,a=uint16(100*rand(i,j));b=a+1;jinvoke_db(test, "F", "a");if a <> b then,disp("bug");end;end;end	
for i=1:N,for j=1:N,a=uint32(1000*rand(i,j));b=a+1;jinvoke_db(test, "F", "a");if a <> b then,disp("bug");end;end;end		
disp("By direct buffers=" + string(toc()));

for m=moc
    jconvMatrixMethod(m);
    tic();
    for i=1:N,for j=1:N,a=rand(i,j);if test.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=int8(10*rand(i,j));if test.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=int16(100*rand(i,j));if test.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=int32(1000*rand(i,j));if test.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=uint8(10*rand(i,j));if test.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=uint16(100*rand(i,j));if test.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=uint32(1000*rand(i,j));if test.F(a) <> a then,disp("bug");end;end;end
    disp("By copy=" + string(toc()));
    
    for i=1:N,for j=1:N,a=string(rand(i,j));if test.F(a) <> a then,disp("bug");end;end;end   
    for i=1:N,for j=1:N,b=rand(i,j);a=jwrapinfloat(b);if abs(test.F(a)-b) > 1E-6 then,disp("bug");end;jremove(a);end;end
    for i=1:N,for j=1:N,b=uint16(1024*rand(i,j));a=jwrapinchar(b);if test.F(a) <> b then,disp("bug");end;jremove(a);end;end
    for i=1:N,for j=1:N,a=(floor(2*rand(i,j))==1);if test.F(a) <> a then,disp("bug");end;end;end
end
    
t = test.new("Hello World")
t.getField()
t.field = "Dlrow Olleh"
t.field
t.getField()
t.setField("Why ??")
t.getField()
t.returnUTF8String("Γεια σας κόσμο")

//Swing test
jimport java.awt.BorderLayout;
jimport java.net.URL;
jimport javax.imageio.ImageIO;
jimport javax.swing.ImageIcon;
jimport javax.swing.JFrame;
jimport javax.swing.JLabel;

url = URL.new("http://www.scilab.org/var/ezwebin_site/storage/images/media/images/puffin/1948-1-eng-GB/puffin.png");
image = ImageIO.read(url);

frame = JFrame.new();
icon = ImageIcon.new(image);
label = JLabel.new(icon);
pane = frame.getContentPane();
pane.add(label, BorderLayout.CENTER);
frame.pack();
frame.setVisible(%T);

jremove frame icon label pane url image BorderLayout URL ImageIO ImageIcon JFrame JLabel
jautoUnwrap(%F);