/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2011 - Allan CORNET
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include <string.h>
#include "gw_helper.h"
#include "MALLOC.h"
#ifdef _MSC_VER
#include "strdup_windows.h"
#endif
/*--------------------------------------------------------------------------*/
static char *methodName = NULL;
static int objectId = 0;
static int envId = 0;
static char isNew = 0;
/*--------------------------------------------------------------------------*/
void setMethodName(const char* _methodName)
{
    if (methodName)
    {
        freeMethodName();
    }
    methodName = strdup(_methodName);
}
/*--------------------------------------------------------------------------*/
char * getMethodName(void)
{
    return methodName;
}
/*--------------------------------------------------------------------------*/
void freeMethodName(void)
{
    if (methodName)
    {
        FREE(methodName);
        methodName = NULL;
    }
}
/*--------------------------------------------------------------------------*/
void setObjectId(int _objectId)
{
    objectId = _objectId;
}
/*--------------------------------------------------------------------------*/
int getObjectId(void)
{
    return objectId;
}
/*--------------------------------------------------------------------------*/
void setIsNew(const char  _isNew)
{
    isNew = _isNew;
}
/*--------------------------------------------------------------------------*/
char getIsNew(void)
{
    return isNew;
}
/*--------------------------------------------------------------------------*/
void setEnvId(int _environemntId)
{
	envId = _environemntId;
}
/*--------------------------------------------------------------------------*/
int getEnvId(void)
{
	return envId;
}
/*--------------------------------------------------------------------------*/