/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
/**
 * Return a _EClass corresponding to given path and return a variable with the same name (if the second var is true).
 * For example, jimport('java.lang.String') will create a variable named String in the stack.
 */
int sci_dnload(char *fname)
{
    SciErr err;
    char *longName = NULL;    
    char *errmsg = NULL;    

    CheckRhs(1, 1);    

    initialization(fname);
    setIsNew(0);

    longName = getSingleString(1, fname);
    if (!longName)
    {
        return 0;
    }

    dotnetload(fname, longName, getAllowReload(), &errmsg);

    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(errmsg);
        freeAllocatedSingleString(longName);
        return 0;
    }   

    freeAllocatedSingleString(longName);    

    return 0;
}
/*--------------------------------------------------------------------------*/
