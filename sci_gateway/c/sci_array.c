/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "Scierror.h"
#include "api_scilab.h"
#include "MALLOC.h"
#include "getSciArgs.h"
#include "noMoreMemory.h"
/*--------------------------------------------------------------------------*/
int sci_array(char *fname)
{
    SciErr err;
    int *addr = NULL;
    char *className = NULL;
    int *args = NULL;
    int i = 0;
    char *errmsg = NULL;
    int ret = 0;

    if (Rhs < 2)
    {
        Scierror(999, "%s: Wrong number of arguments : more than 2 arguments expected\n", fname);
        return 0;
    }

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    className = getSingleString(1, fname);
    if (!className)
    {
        return 0;
    }

    args = (int*)MALLOC(sizeof(int) * (Rhs - 1));
    if (!args)
    {
        freeAllocatedSingleString(className);
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    for (; i < Rhs - 1; i++)
    {
        err = getVarAddressFromPosition(pvApiCtx, i + 2, &addr);
        if (err.iErr)
        {
            FREE(args);
            freeAllocatedSingleString(className);
            printError(&err, 0);
            return 0;
        }

        args[i] = isPositiveIntegerAtAddress(addr);

        if (args[i] == - 1 || args[i] == 0)
        {
            Scierror(999, "%s: A strictly positive integer is expected at position %i\n", fname, i + 2);
            freeAllocatedSingleString(className);
            FREE(args);
            return 0;
        }
    }

    ret = createarray(fname, className, args, Rhs - 1, &errmsg);
    freeAllocatedSingleString(className);
    FREE(args);

    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (!createEnvironmentObjectAtPos(_ENVOBJ, Rhs + 1, ret, fname))
    {
        return 0;
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
