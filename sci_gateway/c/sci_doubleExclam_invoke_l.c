/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
#include "noMoreMemory.h"
#include "gw_helper.h"
/*--------------------------------------------------------------------------*/
/**
 * The function !!_invoke_l is called by !_invoke_ which is called on object.method
 */
int sci_doubleExclam_invoke_l(char *fname)
{
    SciErr err;
    int *addr = NULL;
    int *tmpvar = NULL;
    int *args = NULL;
    int typ = 0;
    int len = 0;
    int *child = NULL;
    int i = 0;
    char *errmsg = NULL;
    char *kindOfInvocation = NULL;
    int ret = 0;

    CheckRhs(1, 1);
    CheckLhs(1, 1);

    initialization(0);

    if (!getCopyOccured()) // if the function is called outside a method context, then return null
    {
		//TODO: get null for environment
        unwrapenv(0, Rhs + 1, getEnvId());
        LhsVar(1) = Rhs + 1;
        PutLhsVar();
        return 0;
    }
    setCopyOccured(0);

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    err = getVarType(pvApiCtx, addr, &typ);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (typ != sci_list)
    {
        Scierror(999, "%s: Wrong type for input argument %i : List expected\n", fname, 3);
        return 0;
    }

    err = getListItemNumber(pvApiCtx, addr, &len);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (!getMethodName())
    {
        return 0;
    }

    if (len == 1)
    {
        err = getListItemAddress(pvApiCtx, addr, 1, &child);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }
        if (isEnvVoid(child))
        {
            len = 0;
        }
    }

    tmpvar = (int*)MALLOC(sizeof(int) * (len + 1));
    if (!tmpvar)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    *tmpvar = 0;
    args = (int*)MALLOC(sizeof(int) * len);
    if (!args)
    {
        FREE(tmpvar);
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    for (; i < len; i++)
    {
        err = getListItemAddress(pvApiCtx, addr, i + 1, &child);
        if (err.iErr)
        {
            removeTemporaryVarsEnv(getEnvId(), tmpvar);
            FREE(args);
            FREE(tmpvar);
            printError(&err, 0);
            return 0;
        }

        args[i] = getIdOfArgEnv(child, fname, tmpvar, 0, i + 1, getEnvId());
        // If args[i] == -1 then we have a scilab variable which cannot be converted in a Java object.
        if (args[i] == - 1)
        {
            removeTemporaryVarsEnv(getEnvId(), tmpvar);
            FREE(args);
            FREE(tmpvar);
            return 0;
        }
    }

    if (getIsNew())
    {
        ret = newinstancenve(getEnvId(), getObjectId(), args, len, &errmsg);
        setIsNew(0);
        kindOfInvocation = "Constructor invocation";
    }
    else
    {
        ret = invokeenv(getEnvId(), getObjectId(), getMethodName(), args, len, &errmsg);
        kindOfInvocation = "Method invocation";
    }

    FREE(args);
    freeMethodName();
    removeTemporaryVarsEnv(getEnvId(), tmpvar);
    FREE(tmpvar);

    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, kindOfInvocation, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (!getIsNew() && getAutoUnwrap())
    {
        if (!unwrapenv(ret, Rhs + 1, getEnvId()))
        {
            if (!createEnvironmentObjectAtPosEnv(_ENVOBJ, Rhs + 1, ret, getEnvId()))
            {
                removescilabobjectenv(getEnvId(), ret);
                return 0;
            }
        }
        else
        {
            removescilabobjectenv(getEnvId(), ret);
        }
    }
    else if (!createEnvironmentObjectAtPosEnv(_ENVOBJ, Rhs + 1, ret, getEnvId()))
    {
        removescilabobject(fname, ret);
        return 0;
    }

    setIsNew(0);

    LhsVar(1) = Rhs + 1;
    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
