/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
int sci_cast(char *fname)
{
    SciErr err;
    int tmpvar[2] = {0, 0};
    int *addr = NULL;
    int idObj = 0;
    int row = 0, col = 0;
    int *id = NULL;
    char *objName = NULL;
    char *errmsg = NULL;
    int ret = 0;
    int envId = -1, envByFunId = -1;

    CheckRhs(2, 2);

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    envId = getIdOfEnvironemnt(addr, fname, 0, 1);
    envByFunId = getEnvironmentId(fname);

    if(envId != envByFunId)
    {
        Scierror(999, ENVIRONMENTERROR, 1, fname);
        return 0;
    }

    idObj = getIdOfArg(addr, fname, tmpvar, 0, 1);
    if (idObj == -1)
    {
        return 0;
    }

    err = getVarAddressFromPosition(pvApiCtx, 2, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (isEnvClass(addr))
    {
        err = getMatrixOfInteger32InList(pvApiCtx, addr, 3, &row, &col, &id);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }
        ret = castwithid(fname, idObj, *id, &errmsg);
    }
    else
    {
        objName = getSingleString(2, fname);
        if (!objName)
        {
            return 0;
        }

        ret = cast(fname, idObj, objName, &errmsg);
        freeAllocatedSingleString(objName);
    }

    removeTemporaryVars(fname, tmpvar);

    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (!createEnvironmentObjectAtPos(_ENVOBJ, Rhs + 1, ret, fname))
    {
        removescilabobject(fname, ret);
        return 0;
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
