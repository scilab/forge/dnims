/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "noMoreMemory.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "MALLOC.h"
#include "gw_helper.h"
/*--------------------------------------------------------------------------*/
int sci_eobj_insert(char *fname)
{
    SciErr err;
    int tmpvarA[2] = {0, 0};
    int tmpvarB[2] = {0, 0};
    int *addr = NULL, *tab = NULL;
    char *errmsg = NULL;
    int idObjA = 0;
    int idObjB = 0;
    int i = 1;
    int type = 0;
    char *fieldName = NULL;
	int envIdA = 0;
	int envIdB = 0;

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, Rhs, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

	envIdA = getIdOfEnvironemnt(addr, fname, 0, Rhs);

    idObjA = getIdOfArgEnv(addr, fname, tmpvarA, 0, Rhs, envIdA);
    if (idObjA == -1)
    {
        return 0;
    }

    err = getVarAddressFromPosition(pvApiCtx, Rhs - 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

	if(isEnvObj(addr) || isEnvClass(addr))
	{
		envIdB = getIdOfEnvironemnt(addr, fname, 0, Rhs - 1);
		if (envIdA != envIdB)
		{
			Scierror(999, "%s: Different environments of target(%i) and inserted(%i) objects:\n", fname, envIdA, envIdB);
			return -1;
		}
	}
	else
	{
		envIdB = envIdA;
	}

    idObjB = getIdOfArgEnv(addr, fname, tmpvarB, 0, Rhs - 1, envIdA);
    if (idObjB == -1)
    {
        return 0;
    }

    /*
     * If all the parameters are integer, we can guess that a matrix insertion is expected
     */
    tab = (int*)MALLOC(sizeof(int) * (Rhs - 2));
    if (!tab)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    for (;i < Rhs - 1; i++)
    {
        err = getVarAddressFromPosition(pvApiCtx, i, &addr);
        if (err.iErr)
        {
            removeTemporaryVarsEnv(getEnvId(), tmpvarB);
            FREE(tab);
            printError(&err, 0);
            return 0;
        }
        tab[i - 1] = isPositiveIntegerAtAddress(addr);
        if (tab[i - 1] == -1)
        {
            FREE(tab);
            tab = NULL;
            break;
        }
    }

    if (tab)
    {
        setarrayelementenv(envIdA, idObjA, tab, Rhs - 2, idObjB, &errmsg);
        FREE(tab);
        removeTemporaryVarsEnv(getEnvId(), tmpvarB);

        if (errmsg)
        {
            Scierror(999, ENVIRONMENTERROR, fname, errmsg);
            FREE(errmsg);
            return 0;
        }

        if (!createEnvironmentObjectAtPosEnv(_ENVOBJ, Rhs + 1, idObjA, envIdA))
        {
            return 0;
        }

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
        return 0;
    }

    fieldName = getSingleString(1, fname);
    if (!fieldName)
    {
        removeTemporaryVarsEnv(envIdA, tmpvarB);
        return 0;
    }

    type = getfieldtypeenv(envIdA, idObjA, fieldName, &errmsg);
    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(errmsg);
        removeTemporaryVarsEnv(envIdA, tmpvarB);
        freeAllocatedSingleString(fieldName);
        return 0;
    }

    if (type == 1)
    {
        setfieldenv(envIdA, idObjA, fieldName, idObjB, &errmsg);
        freeAllocatedSingleString(fieldName);
        removeTemporaryVarsEnv(envIdA, tmpvarB);

        if (errmsg)
        {
            Scierror(999, ENVIRONMENTERROR, fname, errmsg);
            FREE(errmsg);
            return 0;
        }

        if (!createEnvironmentObjectAtPosEnv(_ENVOBJ, Rhs + 1, idObjA, envIdA))
        {
            return 0;
        }

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
        return 0;
    }

    Scierror(999, "%s: No field named %s\n", fname, fieldName);
    freeAllocatedSingleString(fieldName);

    removeTemporaryVarsEnv(envIdA, tmpvarB);
    return 0;
}
/*--------------------------------------------------------------------------*/
