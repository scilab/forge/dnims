function build_gateway_c()
  LD_FLAGS = ""

  here = get_absolute_file_path("builder_gateway_c.sce");
  gw_src_c = ["gw_helper.c", ..
          "sci_exists.c", ..
  	      "sci_jenableTrace.c", ..
  	      "sci_jdisableTrace.c", ..
              "sci_wrapinfloat.c", ..
              "sci_wrapinchar.c", ..
              "sci_wrap.c", ..
              "sci_unwraprem.c", ..
              "sci_unwrap.c", ..
              "sci_setfield.c", ..
              "sci_remove.c", ..
              "sci_eobj_print.c", ..
              "sci_eobj_insert.c", ..
              "sci_eobj_extract.c", ..
              "sci_newInstance.c", ..
              "sci_invoke_lu.c", ..
              "sci_jinvoke_db.c", ..
              "sci_invoke.c", ..
              "sci_init.c", ..
              "sci_import.c", ..
              "sci_getrepresentation.c", ..
              "sci_getmethods.c", ..
              "sci_getfields.c", ..
              "sci_getfield.c", ..
              "sci_getclassname.c", ..
              "sci_deff.c", ..
              "sci_convMatrixMethod.c", ..
              "sci_compile.c", ..
              "sci_eclass_extract.c", ..
              "sci_cast.c", ..
              "sci_autoUnwrap.c", ..
              "sci_array.c", ..
              "sci_allowClassReloading.c", ..
              "sci_doubleExclam_invoke_l.c", ..
              "sci_displayEObj.c", ..
              "sci_getenvironmentname.c"];

  if getos() == "Windows" then
    gw_src_c = [gw_src_c, ..
                "sci_dnload.c", ..
                "dllMain.c"];
  end

  CFLAGS = "-I" + fullpath(here + "../../src/include/") + " " + "-I" + here;
  CFLAGS = CFLAGS + " -I" + fullpath(here + "../../src/c/");
  LDFLAGS = "";

  gw_table = ["jdeff" "sci_deff";
              "jexists" "sci_exists";
  	      "jenableTrace" "sci_jenableTrace";
  	      "jdisableTrace" "sci_jdisableTrace";
              "jcast" "sci_cast";
              "jarray" "sci_array";
              "jwrap" "sci_wrap";
              "jwrapinfloat" "sci_wrapinfloat";
              "jwrapinchar" "sci_wrapinchar";
              "jimport" "sci_import";
              "jnewInstance" "sci_newInstance";
              "jinvoke" "sci_invoke";
              "jinvoke_db" "sci_jinvoke_db";
              "jsetfield" "sci_setfield";
              "jgetfield" "sci_getfield";
              "jgetfields" "sci_getfields";
              "junwrap" "sci_unwrap";
              "junwraprem" "sci_unwraprem";
              "jremove" "sci_remove";
              "jgetmethods" "sci_getmethods";
              "jgetrepresentation" "sci_getrepresentation";
              "jgetclassname" "sci_getclassname";
              "jinit" "sci_init";
              "jcompile" "sci_compile";
              "!!_invoke_l" "sci_doubleExclam_invoke_l";
              "%_EObj_e" "sci_eobj_extract";
              "%_i__EObj" "sci_eobj_insert";
              "%_EClass_e" "sci_eclass_extract";
              "%_EObj_p" "sci_eobj_print";
              "%_EClass_p" "sci_eobj_print";
              "jautoUnwrap" "sci_autoUnwrap";
              "jconvMatrixMethod" "sci_convMatrixMethod";
              "jallowClassReloading" "sci_allowClassReloading";
              "invoke_lu" "sci_invoke_lu";
              "getenvironmentname" "sci_getenvironmentname"]

  if getos() == "Windows" then
    gw_table = [gw_table;
              "dndeff" "sci_deff";
              "dnexists" "sci_exists";
              "dncast" "sci_cast";
              "dnarray" "sci_array";
              "dnwrap" "sci_wrap";
              "dnwrapinfloat" "sci_wrapinfloat";
              "dnwrapinchar" "sci_wrapinchar";
              "dnimport" "sci_import";
              "dnnewInstance" "sci_newInstance";
              "dninvoke" "sci_invoke";
              "dnsetfield" "sci_setfield";
              "dngetfield" "sci_getfield";
              "dngetfields" "sci_getfields";
              "dnunwrap" "sci_unwrap";
              "dnunwraprem" "sci_unwraprem";
              "dnremove" "sci_remove";
              "dngetmethods" "sci_getmethods";
              "dngetrepresentation" "sci_getrepresentation";
              "dngetclassname" "sci_getclassname";
              "dninit" "sci_init";
              "dncompile" "sci_compile";
              "dnload" "sci_dnload";
              "dnloadfrom" "sci_dnload"]
  end

  // PutLhsVar managed by user in sci_sum and in sci_sub
  // if you do not this variable, PutLhsVar is added
  // in gateway generated (default mode in scilab 4.x and 5.x)
  WITHOUT_AUTO_PUTLHSVAR = %t;
  
  tbx_build_gateway("gw_jims_c", gw_table, gw_src_c, here, "", LDFLAGS, CFLAGS);
  
endfunction

build_gateway_c();
clear build_gateway_c;
