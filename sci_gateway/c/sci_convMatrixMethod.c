/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
/**
 *                            [ 1 2 3 ]
 * In Scilab a matrix such as [ 4 5 6 ] is stored [1 4 2 5 3 6] (column by column).
 * In Java the same matrix is stored as [->[1 2 3] ->[4 5 6]] (array of pointers).
 * When methodOfConv == 0 (or 'cr'), the array [1 4 2 5 3 6] is passed as it is and Java creates the array [->[1 4] ->[2 5] ->[3 6]]
 * When methodOfConv == 1 (or 'rc'), the array [1 4 2 5 3 6] is passed as [1 2 3 4 5 6] and converted into [->[1 2 3] ->[4 5 6]]
 */
int sci_convMatrixMethod(char *fname)
{
    char *method = NULL;
    int rc = 0;
    int cr = 0;

    CheckRhs(0, 1);

    setCopyOccured(0);
    initialization(NULL);
    setIsNew(0);

    if (Rhs == 0)
    {
        SciErr err;
        char *meth = "cr";
        if (getMethodOfConv())
        {
            meth = "rc";
        }

        err = createMatrixOfString(pvApiCtx, Rhs + 1, 1, 1, (const char * const *)&meth);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
        return 0;
    }

    method = getSingleString(1, fname);
    if (!method)
    {
        return 0;
    }

    rc = strcmp(method, "rc");
    cr = strcmp(method, "cr");

    if (strlen(method) != 2 || (rc && cr))
    {
        Scierror(999, "%s: The argument must \'rc\' or \'cr\'\n", fname);
        FREE(method);
        return 0;
    }

    if (!cr)
    {
        setMethodOfConv(0);
    }
    else
    {
        setMethodOfConv(1);
    }

    freeAllocatedSingleString(method);

    LhsVar(1) = 0;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
