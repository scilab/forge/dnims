/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
#include "gw_helper.h"
/*--------------------------------------------------------------------------*/
int sci_eclass_extract(char *fname)
{
    SciErr err;
    int tmpvar[2] = {0, 0};
    int *addr = NULL;
    char *errmsg = NULL;
    int idObj = 0;
	int idEnv = 0;
    char *fieldName = NULL;
    int type = 0;

    CheckRhs(2, 2);

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 2, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    idObj = getIdOfArg(addr, fname, tmpvar, 0, 2);
    if (idObj == -1)
    {
        return 0;
    }

	idEnv = getIdOfEnvironemnt(addr, fname, 0, 2);
	if (idEnv == -1)
	{
		return 0;
	}

    fieldName = getSingleString(1, fname);
    if (!fieldName)
    {
        return 0;
    }


    if (!strcmp(fieldName, "new"))
    {
        setIsNew(1);
    }
    else
    {
        type = getfieldtypeenv(idEnv, idObj, fieldName, &errmsg);
        if (errmsg)
        {
            Scierror(999, ENVIRONMENTERROR, fname, errmsg);
            FREE(errmsg);
            removeTemporaryVarsEnv(idEnv, tmpvar);
            freeAllocatedSingleString(fieldName);
            return 0;
        }
    }

    removeTemporaryVarsEnv(idEnv, tmpvar);

    if (!type || getIsNew())
    {
        setMethodName(fieldName);
        setObjectId(idObj);
		setEnvId(idEnv);
        copyInvocationMacroToStack(Rhs);
        LhsVar(1) = Rhs;
        PutLhsVar();
        return 0;
    }
    else if (type == 1)
    {
        type = getfieldenv(idEnv, idObj, fieldName, &errmsg);
        freeAllocatedSingleString(fieldName);

        if (errmsg)
        {
            Scierror(999, ENVIRONMENTERROR, fname, errmsg);
            FREE(errmsg);
            return 0;
        }

        if (!createEnvironmentObjectAtPosEnv(_ENVOBJ, Rhs + 1, type, idEnv))
        {
            return 0;
        }

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
    }
    else
    {
        Scierror(999, "%s: No field or method named %s\n", fname, fieldName);
        freeAllocatedSingleString(fieldName);
    }

    return 0;
}
/*--------------------------------------------------------------------------*/
