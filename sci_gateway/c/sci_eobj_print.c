/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "sciprint.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "MALLOC.h"
/*--------------------------------------------------------------------------*/
int sci_eobj_print(char *fname)
{
    SciErr err;
    int tmpvar[2] = {0, 0};
    char *errmsg = NULL;
    char* rep = NULL;
    int *addr = NULL;
    int idObj = 0;
	int idEnv = 0;

    CheckRhs(1, 1);

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    idObj = getIdOfArg(addr, fname, tmpvar, 0, 1);
    if (idObj == -1)
    {
        return 0;
    }

	idEnv = getIdOfEnvironemnt(addr, fname, 0, 1);
	if (idEnv == -1)
	{
		return 0;
	}

    rep = getrepresentationenv(idEnv, idObj, &errmsg);
    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(rep);
        FREE(errmsg);
        return 0;
    }

    sciprint("%s\n", rep);
    FREE(rep);

    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
